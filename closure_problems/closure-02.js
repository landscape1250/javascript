function makeWorker() {
    name = "Pete";

    return function () {
        alert(name);
    };
}

var name = "John";

// create a function
let work = makeWorker();

// call it
work(); // Pete