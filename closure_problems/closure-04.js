function makeWorker() {
    let name = "Pete";

    return function () {
        alert(name);
    };
}

// create a function
let work = makeWorker();

// call it
work(); // Pete