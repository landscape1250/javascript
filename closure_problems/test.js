function setVar() {
    // inside this function we have a new scope
    // so this variable, declared in this function's scope, won't be available outside the function
    var varInFunction = 'inside a function';
}
setVar();
console.log(varInFunction);
// ReferenceError: varInFunction is not defined