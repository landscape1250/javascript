// Blocking or Synchronous code for reading files
'use strict';

const CHARSET = 'utf-8';

var fs = require('fs');

var contents = fs.readFileSync('users', CHARSET);

console.log(contents);

console.log("Hello Node\n");

contents = fs.readFileSync('hosts', CHARSET);

console.log(contents);

console.log("Hello again!");


/*---Output---*
hemanth
lenord
monkey
cat
Hello Node

192.168.1.1
127.0.0.1
255.255.255.0
Hello again!
*---*/