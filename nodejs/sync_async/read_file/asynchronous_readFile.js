// Non-Blocking or Asynchronous code for reading files
'use strict';

const CHARSET = 'utf-8';

var fs = require('fs');

var contents = fs.readFile('./users', CHARSET, function (err, contents) {
    console.log(contents);
});
console.log("Hello Node\n");


contents = fs.readFile('./hosts', CHARSET, function (err, contents) {
    console.log(contents);
});
console.log("Hello again!");


/*---Output---*
Hello Node

Hello again!
hemanth
lenord
monkey
cat
192.168.1.1
127.0.0.1
255.255.255.0
*---*/