var age = 25;

function localAge() {
    age = 8;
}

console.log('age: ' + age); // 25
localAge();
console.log('age: ' + age); // 8
