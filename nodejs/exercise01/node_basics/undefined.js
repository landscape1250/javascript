var name = undefined;

var x;

console.log(name);
console.log(x);


var y = 'ABS';
console.log(y);

y = undefined;

console.log(y);// undefined


function doNothing() {

}

console.log(doNothing());// undefined

function add() {
    var x = 1 + 2;
}

console.log(add());//undefined


///
if (typeof x === 'undefined') {
    console.log('x is undefined!');
}


if (typeof x01 === 'undefined') {
    console.log('x01 is undefined!');
}


///
function greetUser(name) {
    if (typeof name === 'undefined') {
        console.log('Hello world.');
    } else {
        console.log('Hello ' + name + '!');
    }
}

greetUser('Smith'); //Hello Smith!

greetUser(); // Hello world.



