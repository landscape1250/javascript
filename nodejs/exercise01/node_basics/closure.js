function greetMaker(name) {
    function greet() {
        console.log('Hello ' + name + '!');
    }

    return greet;
}

var greetSmith = greetMaker('Smith');
greetSmith(); // Hello Smith!
greetSmith();// Hello Smith!
greetSmith();// Hello Smith!

var greetWorld = greetMaker('world');
greetWorld();//Hello world!

// add number

function createAdder(baseNumber) {
    return function (numberToAdd) {
        return baseNumber + numberToAdd;
    }
}

var addTen = createAdder(10);
console.log(addTen(2)); // 12
console.log(addTen(6));// 16
