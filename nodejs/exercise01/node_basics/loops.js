// count Down while
function countDownWhile(start, stop) {
    while (start >= stop) {
        console.log('countDownWhile: ' + start--);
    }
}

countDownWhile(10, 0);

// count down for
// function countDownFor(start, stop) {
//     for (var i = start; i >= stop; i--) {
//         console.log('countDownFor: ' + i);
//     }
// }

function countDownFor(start, stop) {
    for (; start >= stop; start--) {
        console.log('countDownFor: ' + start);
    }
}


countDownFor(20, 10);