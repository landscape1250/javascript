
var grades = [90, 50];

grades.push(80); // add at the end of array

console.log(grades); // [ 90, 50, 80 ]


grades.unshift(70); // add at the head of array

console.log(grades); // [ 70, 90, 50, 80 ]


// remove the last
grades.pop();
console.log(grades); // [ 70, 90, 50 ]


// remove the first
grades.shift();
console.log(grades); // [ 90, 50 ]


grades.push(100);

// grades.forEach((grade) -> {
//     console.log(grade);
// })
// ;