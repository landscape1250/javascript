var accounts = [];


function createAccount(account) {
    accounts.push(account);
    return account;
}

function getAccount(username) {
    for (var i = 0; i < accounts.length; i++) {
        if (accounts[i].username === username) {
            return accounts[i];
        }
    }
    return null;
}

function deposit(account, amount) {
    if (typeof  amount === 'number') {
        account.balance += amount;
    } else {
        console.error('deposit failed, amount is not a number.');
    }
}

function withdraw(account, amount) {
    if (typeof  amount === 'number') {
        account.balance -= amount;
    } else {
        console.error('withdraw failed, amount is not a number.');
    }
}

function getBalance(account) {
    return account.balance;
}


function createBalanceGetter(account) {
    return function () {
        return account.balance;
    }
}


var smithAccount = createAccount({
    username: 'Smith',
    balance: 0
});

deposit(smithAccount, 120);
withdraw(smithAccount, 'a string');

var smith2Account = getAccount('Smith');
var getSmith2Balance = createBalanceGetter(smith2Account);

console.log(getSmith2Balance());
