const request = require('request');
const queryString = require('query-string');

//create an object to send as POST data
var postData = {
    "questionList": [
        {
            "answer": "string (optional)",
            "code": "string (optional)",
            "text": "string (optional)",
            "type": "string (optional)"
        }
    ]
};

request.post({
    uri: 'http://apis.ghixqa.com:8080/ms-bot/api/account/123/resetlink',
    headers: {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8'
    },
    body: queryString.stringify(postData)
}, function (err, res, body) {
    console.log(body);
    console.log(res.statusCode);
});


/*--- Response-*

$ node generatePasswordResetLink.js

https://bit.ly/23#s5
200


*--*/