const request = require('request');

const options = {
    url: 'http://apis.ghixqa.com:8080/ms-bot/api/account/123/questions',
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8'
    }
};

request(options, function (err, res, body) {
    var json = JSON.parse(body);
    // console.log(json.questionList);
    console.log(json.questionList[0].code);
    console.log(json.questionList[0].text);
});


/*--- Response-*

$ node generateAuthenticationQuestions.js

{ questionList:
   [ { code: 'FIRSTNAME',
       type: 'STRING',
       text: 'What is your first name?',
       answer: null },
     { code: 'LASTNAME',
       type: 'STRING',
       text: 'What is your last name?',
       answer: null },
     { code: 'DOB',
       type: 'DOB',
       text: 'What is your Date of Birth?',
       answer: null },
     { code: 'SSN',
       type: 'INT',
       text: 'What is last 4 digits of your SSN?',
       answer: null },
     { code: 'ZIPCODE',
       type: 'INT',
       text: 'What is your zip?',
       answer: null } ] }
statusCode:  200
headers:  { 'x-application-context': 'ms-bot:8080',
  'content-type': 'application/json;charset=UTF-8',
  'transfer-encoding': 'chunked',
  date: 'Thu, 10 Aug 2017 20:53:23 GMT',
  connection: 'close' }


  *---*/