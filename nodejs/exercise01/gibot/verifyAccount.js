const request = require('request');

const options = {
    url: 'http://apis.ghixqa.com:8080/ms-bot/api/account/4088889999/email001@getinsured.com/verify',
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8'
    }
};

request(options, function (err, res, body) {
    var json = JSON.parse(body);
    console.log(json);
    console.log("statusCode: ", res.statusCode); // <======= Here's the status code
    console.log("headers: ", res.headers);
});


/*--- Response-*

$ node verifyAccount.js
123
statusCode:  200
headers:  { 'x-application-context': 'ms-bot:8080',
  'content-type': 'application/json;charset=UTF-8',
  'transfer-encoding': 'chunked',
  date: 'Thu, 10 Aug 2017 18:42:42 GMT',
  connection: 'close' }

  *---*/