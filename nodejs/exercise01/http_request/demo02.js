const request = require('request');

const options = {
    url: 'http://www.filltext.com/?rows=10&fname={firstName}&lname={lastName}&pretty=true',
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'User-Agent': 'my-reddit-client'
    }
};

request(options, function (err, res, body) {
    var json = JSON.parse(body);
    console.log(json);
});


/*--- Response-*
$ node demo02.js

    [ { fname: 'Lontay', lname: 'Dellinger' },
  { fname: 'Chanel', lname: 'Templeman' },
  { fname: 'Janice', lname: 'Psarros' },
  { fname: 'Alek', lname: 'Bettencourt' },
  { fname: 'Gloria', lname: 'Molina' },
  { fname: 'Kenneth', lname: 'Marr' },
  { fname: 'Irma', lname: 'Shah' },
  { fname: 'Jasper', lname: 'Azcunaga' },
  { fname: 'Cerelia', lname: 'Mcnally' },
  { fname: 'LuAnn', lname: 'Garayan' } ]

  *---*/