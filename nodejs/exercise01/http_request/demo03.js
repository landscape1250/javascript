const request = require('request');

const options = {
    url: 'http://www.filltext.com/?rows=10&fname=Will&lname=Smith&pretty=true',
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'User-Agent': 'my-reddit-client'
    }
};

request(options, function (err, res, body) {
    var json = JSON.parse(body);
    console.log(json);
});


/*--- Response-*
$ node demo03.js
    [ { fname: 'Will', lname: 'Smith' },
    { fname: 'Will', lname: 'Smith' },
    { fname: 'Will', lname: 'Smith' },
    { fname: 'Will', lname: 'Smith' },
    { fname: 'Will', lname: 'Smith' },
    { fname: 'Will', lname: 'Smith' },
    { fname: 'Will', lname: 'Smith' },
    { fname: 'Will', lname: 'Smith' },
    { fname: 'Will', lname: 'Smith' },
    { fname: 'Will', lname: 'Smith' } ]

  *---*/