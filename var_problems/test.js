if (true) {
  // this if statement doesn't create a new scope
  // so varInIf is available in the global scope
  var varInIf = 'inside an if statement';
}
console.log(varInIf);  // logs 'inside an if statement'