var a = [];
(function () {
    'use strict';
    for (var i = 0; i < 5; ++i) { // *** `let` works as expected ***
        a.push(function () { return i; });
    }
}());
console.log(a.map(function (f) { return f(); }));

/* out:

Array [ 5, 5, 5, 5, 5 ]

*/