if (true) {
  var test_var = 'var'; // use "var" instead of "let"
  let test_let = 'let';
}

console.log(test_var); // 'var', the variable lives after if
console.log(test_let);