var a = [];
(function () {
    'use strict';
    for (let i = 0; i < 5; ++i) {
        a.push(i);
    }
}());
console.log(a);

/* out:

Array [ 0, 1, 2, 3, 4 ]

*/