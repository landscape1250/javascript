function getValue() {
    for (var i = 0; i < 10; i++) {
        // ...
    }
    console.log(i);
}

getValue();

console.log(i); // 10, "i" is visible after loop, it's a global variable