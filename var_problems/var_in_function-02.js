function sayHi_1() {
    phrase = "sayHi_1";

    console.log(phrase);

    var phrase;
}


function sayHi_2() {
    var phrase;

    phrase = "sayHi_2";

    console.log(phrase);
}


function sayHi_3() {
    phrase = "sayHi_3"; // (*)

    if (false) {
        var phrase;
    }

    console.log(phrase);
}


function sayHi_4() {
    phrase = "sayHi_4"; // (*)

    console.log(phrase);
}

sayHi_1();
sayHi_2();
sayHi_3();
sayHi_4();