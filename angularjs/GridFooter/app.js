var app = angular.module('app', ['ngAnimate', 'ngTouch', 'ui.grid', 'ui.grid.cellNav', 'ui.grid.edit', 'ui.grid.resizeColumns', 'ui.grid.pinning', 'ui.grid.selection', 'ui.grid.moveColumns']);

app.controller('MainCtrl', ['$scope', '$http', 'uiGridConstants', function ($scope, $http, uiGridConstants) {
    var today = new Date();
    var nextWeek = new Date();
    nextWeek.setDate(nextWeek.getDate() + 7);
    $scope.genders = [{id: 1, value: 'male'}, {id: 2, value: 'female'}];
    $scope.linetotal = 0;
    $scope.gridOptions = {

        enableFiltering: true,
        showGridFooter: true,
        showColumnFooter: false,
        gridFooterTemplate: "<div class=\"ui-grid-footer-info ui-grid-grid-footer\"><span>{{'search.totalItems' | t}} {{grid.rows.length}}</span><span ng-if=\"grid.renderContainers.body.visibleRowCache.length !== grid.rows.length\" class=\"ngLabel\">({{\"search.showingItems\" | t}} {{grid.renderContainers.body.visibleRowCache.length}})</span><br> <span>Line Total: ${{grid.appScope.linetotal|number}}</span><br><span>The Avg Total: $ {{grid.appScope.linetotal/2|number}}</span></div>",
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
            console.log(gridApi.grid.renderContainers.body.visibleRowCache);
            console.log("init api");
            //console.log($scope.gridApi.core.getVisibleRows($scope.gridApi.grid));
            $scope.gridApi.core.on.rowsRendered($scope, function (grid, sortColumns) {
                //console.log(grid);
                $scope.linetotal = _.reduce(gridApi.grid.renderContainers.body.visibleRowCache, function (a, b) {
                    return a + b.entity.age;
                }, 0);

            });
        },
        columnDefs: [
            {field: 'name', width: 100},
            {
                field: 'gender', width: 100, cellFilter: "mapGender", enableFiltering: true, filter: {
                term: $scope.term //DOES NOT WORK... BUT WHY
            }
            },
            {field: 'company', width: 100, enableFiltering: false},
            {field: 'email', width: 100, enableFiltering: false},
            {field: 'phone', width: 100, enableFiltering: false},
            {
                field: 'age',
                width: 130,
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true
            },//,footerCellTemplate: "<div class=\"ui-grid-cell-contents\" >Total: ${{$scope.gridApi.grid.columns[6].aggregationValue|number}}</div>"
            {field: 'mixedDate', cellFilter: 'date', width: '15%', enableFiltering: false},
            {field: 'company', width: 100, enableFiltering: false},
            {field: 'email', width: 100, enableFiltering: false},
            {field: 'phone', width: 100, enableFiltering: false},
            {field: 'company', width: 100, enableFiltering: false},
            {field: 'email', width: 100, enableFiltering: false},
            {field: 'phone', width: 100, enableFiltering: false},
            {field: 'company', width: 100, enableFiltering: false},
            {field: 'email', width: 100, enableFiltering: false},
            {field: 'phone', width: 100, enableFiltering: false},
            {field: 'company', width: 100, enableFiltering: false},
            {field: 'email', width: 100, enableFiltering: false},
            {field: 'phone', width: 100, enableFiltering: false}
        ]
    };
    function totalOfLineItems() {
        console.log($scope.gridOptions.data);
        $scope.lineTotal = _.reduce($scope.gridOptions.data, function (a, b) {
            return a + b.age
        }, 0);
    }

    $scope.filterGender = function () {
        console.log($scope.gridApi.grid.columns);
        $scope.gridApi.grid.columns[2].filter.term = $scope.term;
        //$scope.gridApi.grid.queueGridRefresh();
    };

    $http.get('https://cdn.rawgit.com/angular-ui/ui-grid.info/gh-pages/data/500_complex.json')
        .success(function (data) {
            $scope.gridOptions.data = data;
            $scope.gridOptions.data[0].age = -5;

            data.forEach(function addDates(row, index) {
                row.mixedDate = new Date();
                row.mixedDate.setDate(today.getDate() + ( index % 14 ));
                row.gender = row.gender === 'male' ? '1' : '2';
            });
        });
}])

    .filter('mapGender', function () {
        var genderHash = {
            1: 'male',
            2: 'female'
        };

        return function (input) {
            if (!input) {
                return '';
            } else {
                return genderHash[input];
            }
        };
    });
