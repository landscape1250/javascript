angular.module('myApp', ['report.multiselect'])
  .controller('DemoController', ['$scope', '$http', '$sce',
    function($scope, $http, $sce) {
    
    $scope.multiselect = {
      selected: [],
      options: [],
      config: {
        hideOnBlur: false,
        showSelected: false,
        itemTemplate: function(item){
          return $sce.trustAsHtml(item.name+' ('+item.email+')');
        },
        labelTemplate: function(item){
          return $sce.trustAsHtml(item.name);
        }
      }
    };
    
    $scope.displayUsers = function(){
      return $scope.multiselect.selected.map(function(each){
        return each.name;
      }).join(', ');
    };
    
    $http.get('http://jsonplaceholder.typicode.com/users')
      .success(function(data){
        // Prepare the fake data
        $scope.multiselect.options = data.map(function(item){
          return {
            name: item.name,
            email: item.email,
            id: item.id
          };
        });
        
      }).error(function(err){
        console.error(err);
      });
      
  }]);