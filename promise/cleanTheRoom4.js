let cleanRoom = () => {
  return new Promise((resolve, reject) => {
    resolve('Cleaned the room');
  });
};

let removeGarbage = (message) => {
  return new Promise((resolve, reject) => {
    resolve(message + ' remove garbage');
  });
};

let winIcecream = (message) => {
  return new Promise((resolve, reject) => {
    resolve(message + ' won Icecream');
  });
};


// once any of them done, do something
Promise.race([cleanRoom(), removeGarbage(), winIcecream()]).then(
() => {
   console.log('one of them is finished!');
});


