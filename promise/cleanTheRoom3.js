let cleanRoom = () => {
  return new Promise((resolve, reject) => {
    resolve('Cleaned the room');
  });
};

let removeGarbage = (message) => {
  return new Promise((resolve, reject) => {
    resolve(message + ' remove garbage');
  });
};

let winIcecream = (message) => {
  return new Promise((resolve, reject) => {
    resolve(message + ' won Icecream');
  });
};


// once all of them done, do something
Promise.all([cleanRoom(), removeGarbage(), winIcecream()]).then(
() => {
   console.log('All finished!');
});


